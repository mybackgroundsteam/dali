package maestro.dali;

import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class Dali {

    private Context mContext;

    Dali(Context context) {
        mContext = context.getApplicationContext();
    }

    public DaliRequest load(Object object) {
        return new DaliRequest(this, mContext, object);
    }

    /**
     * Getters
     **/

    private static volatile Dali instance;

    public static synchronized Dali initialize(Context context) {
        if (instance == null) {
            synchronized (Dali.class) {
                if (instance == null) {
                    instance = new Dali(context);
                }
            }
        }
        return instance;
    }

    public static Dali get(Object object) {
        if (instance == null) {
            Context context = null;
            if (object instanceof Context) {
                context = (Context) object;
            } else if (object instanceof Fragment) {
                context = ((Fragment) object).getContext();
            }
            if (context != null) {
                initialize(context);
            }
        }
        return instance;
    }

}
