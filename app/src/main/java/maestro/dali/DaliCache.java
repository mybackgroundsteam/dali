package maestro.dali;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class DaliCache {

    public static final String TAG = DaliCache.class.getSimpleName();

    private final ArrayList<DaliKey> mKeys = new ArrayList<>();
    private final ArrayList<Object> mObjects = new ArrayList<>();
    private final Object mLock = new Object();

    private SizeOf mSizeOf = new DefSizeOf();
    private OnItemRemovedListener mItemRemovedListener = new DefItemRemovedListener();
    private int mCapacity;
    private int mUsedCapacity;

    public void setCapacity(int capacity) {
        synchronized (mLock) {
            mCapacity = capacity;
            verifyCapacity(0);
        }
    }

    public boolean add(Object object, DaliKey key) {
        if (object == null || key == null) {
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "add fail: " + key + "{" + object + "}");
            }
            return false;
        }
        synchronized (mLock) {
            final int size = mSizeOf.size(object);
            verifyCapacity(size);
            mKeys.add(key);
            mObjects.add(object);
            mUsedCapacity += size;
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "add: " + key + ", capacity: " + mUsedCapacity + "/" + mCapacity + ", {" + object + "}");
            }
            return true;
        }
    }

    public Object get(DaliKey key) {
        if (key == null) {
            Log.e(TAG, "get fail: " + key);
            return null;
        }
        synchronized (mLock) {
            int index = mKeys.indexOf(key);
            if (index != -1) {
                Log.e(TAG, "get: " + key);
                return mObjects.get(index);
            } else if (DaliUtils.DEBUG) {
                Log.e(TAG, "get fail: " + key);
            }
            return null;
        }
    }

    public boolean remove(DaliKey key) {
        if (key == null) {
            Log.e(TAG, "remove fail: " + key);
            return false;
        }
        synchronized (mLock) {
            int index = mKeys.indexOf(key);
            if (index != -1) {
                DaliKey removedKey = mKeys.remove(index);
                Object removedObject = mObjects.remove(index);
                mUsedCapacity -= mSizeOf.size(removedObject);
                mItemRemovedListener.onItemRemoved(removedObject);
                if (DaliUtils.DEBUG) {
                    Log.e(TAG, "remove: " + removedKey + ", capacity: " + mUsedCapacity + "/" + mCapacity + ", {" + removedObject + "}");
                }
                return true;
            }
            return false;
        }
    }

    private void verifyCapacity(int adjustSize) {
        if (DaliUtils.DEBUG) {
            Log.e(TAG, "verifyCapacity...");
        }
        while (mUsedCapacity + adjustSize > mCapacity) {
            if (mKeys.size() == 0) {
                break;
            }
            remove(mKeys.get(0));
        }
    }

    public void clear() {
        synchronized (mLock) {
            mUsedCapacity = 0;
            mKeys.clear();
            mObjects.clear();
        }
    }

    /**
     * Constructor and singletons
     **/

    private static DaliCache instance;

    public static synchronized DaliCache get() {
        if (instance == null) {
            synchronized (DaliCache.class) {
                if (instance == null) {
                    instance = new DaliCache();
                }
            }
        }
        return instance;
    }

    DaliCache() {
    }

    public interface SizeOf {
        int size(Object object);
    }

    public interface OnItemRemovedListener {
        void onItemRemoved(Object object);
    }

    public class DefSizeOf implements SizeOf {

        @Override
        public int size(Object object) {
            if (object instanceof Bitmap) {
                return ((Bitmap) object).getByteCount() / 1024;
            }
            return 0;
        }

    }

    public class DefItemRemovedListener implements OnItemRemovedListener {

        @Override
        public void onItemRemoved(Object object) {
            if (object instanceof Bitmap) {
                Bitmap bitmap = (Bitmap) object;
                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
        }

    }

}
