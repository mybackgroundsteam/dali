package maestro.dali;

import android.util.Pair;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class DaliKey extends Pair<String, String> {

    public DaliKey(String primary) {
        super(primary, null);
    }

    public DaliKey(String primary, String secondary) {
        super(primary, secondary);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[@" + Integer.toHexString(hashCode()) + "]: " + first + ", " + second;
    }
}