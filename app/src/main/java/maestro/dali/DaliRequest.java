package maestro.dali;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class DaliRequest implements Runnable {

    public static final String TAG = DaliRequest.class.getSimpleName();

    private Context mContext;

    private Dali mDali;
    private DaliKey mKey;
    private Object mLoadObject;
    private Object mTargetObject;
    private DaliProcessor mProcessor;

    private Bitmap.Config mConfig = Bitmap.Config.RGB_565;

    private int mTargetWidth;
    private int mTargetHeight;

    private int mMaxSize = 2560;

    private boolean mCacheInMemory = true;
    private boolean mCacheOnDisk = true;

    private boolean doNotSample = false;
    private boolean isCanceled = false;

    public DaliRequest(Dali dali, Context context, Object loadObject) {
        mDali = dali;
        mContext = context;
        mLoadObject = loadObject;
        mKey = new DaliKey(DaliUtils.keyOfObject(loadObject));
    }

    public DaliRequest noCache() {
        mCacheInMemory = false;
        return this;
    }

    public DaliRequest noDiskCache() {
        mCacheOnDisk = false;
        return this;
    }

    public DaliRequest noSample() {
        doNotSample = true;
        return this;
    }

    public DaliRequest maxSize(int maxSize) {
        mMaxSize = maxSize;
        return this;
    }

    public void cancel() {
        isCanceled = true;
    }

    public Object getLoadObject() {
        return mLoadObject;
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public void run() {
        if (!isCanceled) {
            DaliProcessor processor = getProcessor();
            BufferedInputStream stream = null;
            try {
                stream = new BufferedInputStream(processor.getStream(this));

                BitmapFactory.Options mOptions = new BitmapFactory.Options();
                mOptions.inPreferredConfig = mConfig;
                if (!doNotSample && mTargetWidth > 0 && mTargetHeight > 0) {
                    mOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(stream, null, mOptions);
                    mOptions.inJustDecodeBounds = false;
                }

                Bitmap bitmap = BitmapFactory.decodeStream(stream);
                if (mCacheInMemory) {
                    DaliCache.get().add(bitmap, mKey);
                }
                if (mCacheOnDisk) {
                    DaliDiskCache.get().add(bitmap, mKey.first);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public DaliProcessor getProcessor() {
        return mProcessor == null ? new DefaultDaliProcessor() : mProcessor;
    }

    public void async() {
    }

    public void block() {
    }

    public interface OnLoadListener {
        void onLoadStart(DaliRequest request);

        void onLoadProgress(DaliRequest request);

        void onLoadFinish(DaliRequest request, Bitmap result);

        void onLoadError(DaliRequest request);
    }

}