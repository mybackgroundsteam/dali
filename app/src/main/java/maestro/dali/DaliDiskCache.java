package maestro.dali;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class DaliDiskCache {

    public static final String TAG = DaliDiskCache.class.getSimpleName();

    private final ArrayList<String> mKeys = new ArrayList<>();
    private final ArrayList<File> mFiles = new ArrayList<>();
    private final Object mLock = new Object();
    private final CacheOptions mDefOptions = new CacheOptions();

    {
        mDefOptions.mFormat = Bitmap.CompressFormat.JPEG;
        mDefOptions.mQuality = 100;
    }

    private File mFolder;
    private int mUsedCapacity;
    private int mCapacity;
    private boolean isInitialize = false;

    public static final class CacheOptions {

        private Bitmap.CompressFormat mFormat;
        private int mQuality;
    }

    public void add(Bitmap bitmap, String key) {
        add(bitmap, key, mDefOptions);
    }

    public void add(Bitmap bitmap, String key, CacheOptions options) {
        if (bitmap == null || key == null) {
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "add fail: " + key + ", {" + bitmap + "}");
            }
            return;
        }
        synchronized (mLock) {
            File outFile = new File(mFolder, key);
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(outFile);
                bitmap.compress(options.mFormat, options.mQuality, outStream);
                mKeys.add(key);
                mFiles.add(outFile);
                Log.e(TAG, "add: " + key + ", capacity: " + mUsedCapacity + "/" + mCapacity);
            } catch (FileNotFoundException e) {
                Log.e(TAG, "add fail: " + key);
                e.printStackTrace();
            } finally {
                if (outStream != null) {
                    try {
                        outStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public Bitmap get(String key) {
        if (key == null) {
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "get fail: " + key);
            }
            return null;
        }
        synchronized (mLock) {
            int index = mKeys.indexOf(key);
            if (index != -1) {
                Log.e(TAG, "get: " + key);
                return BitmapFactory.decodeFile(mFiles.get(index).getAbsolutePath());
            } else if (DaliUtils.DEBUG) {
                Log.e(TAG, "get fail: " + key);
            }
            return null;
        }
    }

    public boolean remove(String key) {
        if (key == null) {
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "remove fail: " + key);
            }
            return false;
        }
        synchronized (mLock) {
            int index = mKeys.indexOf(key);
            if (index != -1) {
                String removedKey = mKeys.remove(index);
                File removedFile = mFiles.remove(index);
                if (removedFile.delete()) {
                    mUsedCapacity -= removedFile.length();
                    Log.e(TAG, "removed: " + key + ", capacity: " + mUsedCapacity + "/ " + mCapacity);
                    return true;
                }
            }
            if (DaliUtils.DEBUG) {
                Log.e(TAG, "remove fail: " + key);
            }
            return false;
        }
    }

    private void verifyCapacity(int adjustSize) {
        while (mUsedCapacity + adjustSize > mCapacity) {
            if (mKeys.size() == 0) {
                break;
            }
            remove(mKeys.get(0));
        }
    }

    private void initialize() {
        synchronized (mLock) {
            File[] files = mFolder.listFiles();
            for (File file : files) {
                mKeys.add(file.getName());
                mFiles.add(file);
                mUsedCapacity += file.length();
            }
            verifyCapacity(0);
        }
    }

    DaliDiskCache() {
        initialize();
    }

    private static volatile DaliDiskCache instance;

    public static synchronized DaliDiskCache get() {
        if (instance == null) {
            synchronized (DaliCache.class) {
                instance = new DaliDiskCache();
            }
        }
        return instance;
    }

}
