package maestro.dali;

import java.io.InputStream;

/**
 * Created by Pixolio on 30.04.2016.
 */
public interface DaliProcessor {

    InputStream getStream(DaliRequest request);

}