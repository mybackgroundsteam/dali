package maestro.dali;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Pixolio on 01.05.2016.
 */
public class DefaultDaliProcessor implements DaliProcessor {

    @Override
    public InputStream getStream(DaliRequest request) {
        if (request.getLoadObject() instanceof String) {
            String path = (String) request.getLoadObject();
            if (path.startsWith("http:") || path.startsWith("https:")) {
                try {
                    URLConnection connection = new URL(path).openConnection();
                    return connection.getInputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        } else if (request.getLoadObject() instanceof Integer) {
            return request.getContext().getResources().openRawResource((Integer) request.getLoadObject());
        } else if (request.getLoadObject() instanceof File) {
            try {
                return new FileInputStream(((File) request.getLoadObject()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        } else if (request.getLoadObject() == null) {
            return null;
        }
        throw new UnsupportedOperationException("Unknown source: " + request.getLoadObject());
    }

}
