package maestro.dali;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;

import java.io.File;

/**
 * Created by Pixolio on 30.04.2016.
 */
public class DaliUtils {

    public static final String KEY_LANDSCAPE = "land";
    public static final String KEY_PORTRAIT = "port";
    public static final String KEY_DIVIDER = "_";

    public static boolean DEBUG = false;

    public static String keyOfObject(Object object) {
        if (object instanceof String) {
            return makePathFromUrl((String) object);
        } else if (object instanceof File) {
            return makePathFromUrl(((File) object).getAbsolutePath());
        } else if (object instanceof Uri) {
            return makePathFromUrl(((Uri) object).getPath());
        } else if (object == null) {
            return null;
        }
        throw new UnsupportedOperationException("Unknown object: " + object);
    }

    public static String makePathFromUrl(String _s) {
        String string = _s.replaceAll("[\\\\s.:;&=<>/]", "");
        if (string.length() > 160) {
            string = String.valueOf(string.hashCode());
        }
        return string;
    }

    public static String makeOrientationKey(Context context, String key, String... params) {
        StringBuilder builder = new StringBuilder(key).append(KEY_DIVIDER);
        builder.append(context.getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE ? KEY_LANDSCAPE : KEY_PORTRAIT);
        if (params != null && params.length > 0) {
            for (String str : params) {
                builder.append(KEY_DIVIDER).append(str);
            }
        }
        return builder.toString();
    }

}
